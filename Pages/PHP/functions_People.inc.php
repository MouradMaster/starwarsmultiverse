<?php

/***************************************************PEOPLE*********************************************************************/

//get people
function get_People($name){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM `people` WHERE `name` = ? "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s', $name);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}

//get all People
function get_all_People(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM people "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
        $i=0;
    while($row=$res->fetch_assoc()){
        $people[$i]=$row;
        $i=$i+1;
    }  
    return $people;
    }
}
}
else
return false;
}

//insert People
function insert_People($name,$gender,$height,$mass,$homeworld){
   
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("INSERT INTO `people` (`id`, `name`, `gender`, `height`, `mass`,`homeworld`) VALUES (NULL,?, ?,?, ?,?)")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    
    $stmt->bind_param('ssiii',$name,$gender,$height,$mass,$homeworld);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}


//Edit People
function edit_People($name,$gender,$height,$mass,$homeworld){


    $people=get_People($name);
    $id_people=$people["id"];

    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("UPDATE `people` SET  `name`=?, `gender`=?, `height`=?, `mass`=?, `homeworld`=? WHERE `id`=?")))  {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    }
    else{
    
    $stmt->bind_param('ssiiii',$name,$gender,$height,$mass,$homeworld,$id_people);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    else{
      return true;  
    }

    }

    }

}

//delete People
function delete_People($name){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("DELETE FROM `people` WHERE `name` = ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$name);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
}


//Get Poster People
function get_Poster_People($name){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_people INNER JOIN people ON (pictures_people.id=people.id)  WHERE `name`=?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('s',$name);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();
    return $row;
    }
}
}
else
return false;
}

function get_all_Posters_People(){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM pictures_people INNER JOIN people ON (pictures_people.id=people.id)"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    while($row=$res->fetch_assoc()){
        $i=$row["id"];
        $poster_people[$i]=$row["path"];
    }  
    return $poster_people;
    }
}
}
else
return false;
}

//Change Poster
function change_Poster_People($name,$path){
  
    $people=get_People($name);
    $id_people=$people["id"];

    $row=get_Poster_People($name); 

    if(empty($row)){  
     $mysqli=Connection();
     if(!empty($mysqli)){
         if (!($stmt = $mysqli->prepare("INSERT INTO `pictures_people` (`id_picture`, `id`,`path`) VALUES (NULL, ?, ?)")))  {
     echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
     }
     else{
     $stmt->bind_param('is',$id_people,$path);
 
     if (!$stmt->execute()) {
         echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
         return false;
         }
     else{
       return true;  
     }
 
     }
 
     }

     else 
     return false;
 }

 else{
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("UPDATE pictures_people SET `path` = ?  WHERE `id`= ?"))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('si',$path,$id_people);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
      return true;  
}
}

else
return false;
} 
}


//Claire LEGUILLETE
//Get homeworld
function get_homeworld($id){

    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT * FROM planet WHERE `id` = ? "))) 
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i', $id);

    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
        }
    
    else{
        $res=$stmt->get_result();

    if($res->num_rows == 0){
         return false ;
    }   
    else{
    $row=$res->fetch_assoc();   
    return $row;
    }
}
}

else
return false;
}

//get people
function get_People_info($id){
    $mysqli=Connection();
    if(!empty($mysqli)){
        if (!($stmt = $mysqli->prepare("SELECT * FROM `people` WHERE `id` =?")))
        {
            echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
            return false;
        }

        $stmt->bind_param('i',$id);

        if (!$stmt->execute()) {
            echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
    
        else{
            $res=$stmt->get_result();
            if($res->num_rows == 0){
            return false ;
            }   
            else{
            $row=$res->fetch_assoc();
            return $row;   
            }
        }
    }
}

//Get Picture people thanks to the Id_People
/*function get_Picture_People($id_picture){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT `Lien` FROM Charater_Pictures WHERE `id_Character`= ?")))
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i',$id_picture);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
    }
    
    else{
        $res=$stmt->get_result();

        if($res->num_rows == 0){
            return false ;
        }   
        else{
            $row=$res->fetch_assoc();
            return $row['Lien'];
        }
    }
}
    else
    return false;
}*/



?>