<?php
//Title_page
$title_page="Add Film";

//StyleSheet
$style_file="../CSS/add_Catalog_Style.css";

//Head
include("head.inc.php");
?>
<body>

<?php
//Check Log
if(empty($_SESSION["logged"]) || ($_SESSION["role"]<3))
header('Location:Home.php');
?>

<main>
  <div class="row">
    <div class="col-sm">
    <h3>Fill the blanks :</h3>
</div>
<div class="col-sm">
<!--Error-->
<?php
     
     if(!empty($_SESSION["error_add"])){
       if(isset($_GET['error'])){
        if($_GET['error']==0)
        echo '<span class="alert alert-success"><strong>Success !</strong></span>';
         elseif($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Episode already existed !</strong></span>';
       elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Error Add Film !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
       }

       unset($_SESSION["error_add"]);
      }
            ?>

      </div>
    </div>       
    <form method="POST" action="action_AddCatalog.php">
    <div class="form-group">
          <label for="title">Title :</label>
          <input type="text" class="form-control" placeholder="Enter Title" id="title" name="title" required maxlength="30" required>
        </div>

        <div class="form-group">
          <label for="release_date">Release Date :</label>
          <input type="date" class="form-control" placeholder="Enter Release Date" id="release_date" name="release_date" required maxlength="30" required>
        </div>

        <div class="form-group">
          <label for="episode">Episode :</label>
          <input type="number" class="form-control" id="episode" name="episode" required>
        </div>

        <div class="form-group">
          <label for="opening">Opening :</label>
          <textarea class="form-control" name="opening" id="opening" rows="10" cols="80" required></textarea>
        </div>

       <div class="form-group">
        <label for="poster">Poster : </label><br />
       <select name="poster" id="poster">
       <?php  foreach (glob("../../Pictures/Films/*") as $filename) {
         echo '<option value="'.$filename.'">'.$filename.'</option>';
            }  
         ?>
        <option value="Empty"></option>
       </select>
       </div>

            <br>

        <button type="submit" class="btn btn-primary" name="button" value="Film">Add Film</button>

    </form>


      
      <div><br/><a href="Catalog_Films.php">Back</a></div>
</main>


</body>