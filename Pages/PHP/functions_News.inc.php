<?php
//Get Picture News
function get_Picture_News($id){
    $mysqli=Connection();
    if(!empty($mysqli)){
    if (!($stmt = $mysqli->prepare("SELECT `Lien` FROM  News_Pictures WHERE `id`= ?")))
    {
    echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
    return false;
    }

    $stmt->bind_param('i',$id);


    if (!$stmt->execute()) {
        echo "Echec lors de l’exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
        return false;
    }
    
    else{
        $res=$stmt->get_result();

        if($res->num_rows == 0){
            return false ;
        }   
        else{
            $row=$res->fetch_assoc();
            return $row['Lien'];
        }
    }
}
    else
    return false;
}

?>