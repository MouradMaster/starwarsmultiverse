<?php
//Title_page
$title_page="Add People";

//StyleSheet
$style_file="../CSS/add_Catalog_Style.css";

//Head
include("head.inc.php");
?>
<body>

<?php
//Check Log
if(empty($_SESSION["logged"]) || ($_SESSION["role"]<3))
header('Location:Home.php');
?>

<main>
  <div class="row">
    <div class="col-sm">
    <h3>Fill the blanks :</h3>
</div>
<div class="col-sm">
<!--Error-->
<?php
     
     if(!empty($_SESSION["error_add"])){
       if(isset($_GET['error'])){
        if($_GET['error']==0)
        echo '<span class="alert alert-success"><strong>Success !</strong></span>';
         elseif($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Person already existed !</strong></span>';
       elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Error Add Person !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
       }

       unset($_SESSION["error_add"]);
      }
            ?>

      </div>
    </div>       
    <form method="POST" action="action_AddCatalog.php">
    <div class="form-group">
          <label for="name">Name :</label>
          <input type="text" class="form-control" placeholder="Enter Name" id="name" name="name" required maxlength="30" required>
        </div>

        <div class="form-group">
        <label for="gender">Gender : </label><br />
       <select name="gender" id="gender">
       <option value="M">Male</option>
       <option value="F">Female</option>
       <option value="-">-</option>
       </select>
       </div>
       <br>

        <div class="form-group">
          <label for="height">Height :</label>
          <input type="number" class="form-control" id="height" name="height" required>
        </div>

        <div class="form-group">
          <label for="mass">Mass :</label>
          <input type="mass" class="form-control" id="mass" name="mass" required>
        </div>

       <div>
        <label for="homeworld">HomeWorld : </label><br />
       <select name="homeworld" id="homeworld">
       <?php  foreach ($planets_glob as $homeworld) {
         echo '<option value="'.$homeworld["id"].'">'.$homeworld["name"].'</option>';
            }  
         ?>
         <!--<option value="0">Unknown</option>-->
       </select>
       </div>
       <br>
       <div class="form-group">
        <label for="poster">Poster : </label><br />
       <select name="poster" id="poster">
       <?php  foreach (glob("../../Pictures/People/*") as $filename) {
         echo '<option value="'.$filename.'">'.$filename.'</option>';
            }  
         ?>
         <option value="Empty"></option>
       </select>
       </div>

            <br>

        <button type="submit" class="btn btn-primary" name="button" value="People">Add Person</button>

    </form>


      
      <div><br/><a href="Catalog_Caractere.php">Back</a></div>
</main>


</body>