<?php
//Title_page
$title_page="Users";

//StyleSheet
$style_file="../CSS/ManageUser_Style.css";

//Head
include("head.inc.php");
?>

<body>

<?php
//Check Log
if(empty($_SESSION["logged"]) || $_SESSION["role"]<3)
header('Location:Home.php');
?>


<main>

<?php
if(!empty($_GET["search"])){
  $users=get_all_Users_By($_GET["search"]);
}

?>


<div>
<form action="#" method="GET" id="search">
	<input type="text" name="search" />
	<input type="submit" value="Search" />
</form>
<a href="ManageUser.php">Reset</a>
</div>
<br>

<form action="action_ManageUser.php" method="POST">
<?php
$max=sizeof($users);

if(!empty($users)){
for($i=0;$i<$max;$i++){
if($users[$i]["role"]<$_SESSION["role"]){
    ?>
     <div class="row">
    <div class="col-sm">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" name="users[]" value=<?php echo $users[$i]["username"] ;?> >
      <label class="form-check-label" for="defaultCheck1">
        <?php echo $users[$i]["username"] ;?>       :
      </label>
      </div>
    </div>
    

    <div class="col-sm">
    <div>
      <label>
        <?php 
        if($users[$i]["role"]==2)
        echo "Moderator" ;
        elseif($users[$i]["role"]==1)
        echo "User";
        ?>
      </label>
      </div>

    </div>
</div>

    <?php
    }
}

}
else
echo '<label>Aucun résultat trouvé</label>';
?>

<br>
<?php
if($_SESSION["role"]==3){
echo '<div><button type="submit" class="btn btn-primary" name="button" value="Promote">Promote</button>';
echo '<button type="submit" class="btn btn-primary" name="button" value="Downgrade">Downgrade</button></div>';
}
?>

<br>
<button type="submit" class="btn btn-primary" name="button" value="Delete">Delete</button>
</form>

<br>
<a href="Settings.php">Back</a>
</main>


</body>