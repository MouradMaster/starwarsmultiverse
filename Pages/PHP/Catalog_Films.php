<!--Page de listing de tous les films--> 


<?php
//Title_page
$title_page="Catalog";

//StyleSheet
$style_file="../CSS/CatalogStyle.css";


//Head
include("head.inc.php");
?>


<body>
    <?php
    //Check Log
    if(empty($_SESSION["logged"]))
    header('Location:Login.php');

    //Header
    include("header.inc.php");
    ?>
<br/>
<div style="text-align : center">
<!--Error-->
<?php
     
     if(!empty($_SESSION["error_edit"])){
       if(isset($_GET['error'])){
        if($_GET['error']==0)
        echo '<span class="alert alert-success"><strong>Success !</strong></span>';
         elseif($_GET['error']==1)
       echo '<span class="alert alert-warning"><strong>Error!</strong></span>';
       elseif($_GET['error']==2)
       echo '<span class="alert alert-warning"><strong>Error !</strong></span>';
        else
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
       }

       unset($_SESSION["error_edit"]);
      }

      if(!empty($_SESSION["error_delete"])){
        if(isset($_GET['error'])){
         if($_GET['error']==0)
         echo '<span class="alert alert-success"><strong>Success !</strong></span>';
          elseif($_GET['error']==1)
        echo '<span class="alert alert-warning"><strong>Error!</strong></span>';
        elseif($_GET['error']==2)
        echo '<span class="alert alert-warning"><strong>Error !</strong></span>';
         else
         echo '<span class="alert alert-warning"><strong>Error!</strong></span>';     
        }
 
        unset($_SESSION["error_delete"]);
       }
            ?>

      </div>
      <br>
<?php if($_SESSION["role"] == 3) { ?>
<form action="add_Film.php"  style="text-align : center">
<div>
        <button type="submit" class="btn btn-success" name="button" value="Film">Add More Films</button>
</div>  
</form>
<?php
}
?>
    <br/>

<main>

    <div class="grid-container"> 
        
        <?php
            $max=sizeof($film);

            for($i=0;$i<$max;$i++){
                $title_film=$film[$i]["title"];
                $episode=$film[$i]["episode"];
                $id=$film[$i]["id"];
                if(!empty($poster_film[$id])) $poster=$poster_film[$id]; else $poster="";
        ?>

        <div  class="grid-item">
            
            <a href="FullContent.php?id=<?php echo $i;?>" >
            <h3 style="text-align : center"><?php echo $title_film;?></h3>
            <img src=<?php if(!empty($poster)) echo $poster; ?> class="Affiche" alt="Affiche" >
            </a><br/>
            <?php if($_SESSION["role"] == 3) { ?>
              
            <form action="Edit_Film.php" method="POST"  style="text-align : center">
                    <button type="submit" class="btn btn-primary" name="button" value=<?php echo $episode;?>>Edit</button>
            </form>

            <form action="action_DeleteCatalog.php?category=Film" method="POST"  style="text-align : center">
                    <button type="submit" class="btn btn-danger" name="button" value=<?php echo $episode;?>>Delete</button>
            </form>
            <?php } ?>
        </div>
        <?php } ?>
        </div>            
    <br/>       
            </main>
    <?php 
    include("footer.inc.php");
    ?>
    

    


</body>


    