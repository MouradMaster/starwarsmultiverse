-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 25, 2020 at 10:55 PM
-- Server version: 10.1.26-MariaDB-0+deb9u1
-- PHP Version: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bdd_9_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `Charater_Pictures`
--

CREATE TABLE `Charater_Pictures` (
  `id` int(20) NOT NULL,
  `Lien` varchar(100) NOT NULL,
  `id_Character` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Charater_Pictures`
--

INSERT INTO `Charater_Pictures` (`id`, `Lien`, `id_Character`) VALUES
(1, '../../Pictures/People/AnakinSkywalker.png', 8),
(2, '../../Pictures/People/C-3PO.jpg', 2),
(3, '../../Pictures/People/Chewbacca.png', 9),
(4, '../../Pictures/People/DarthVader.jpg', 4),
(5, '../../Pictures/People/HanSolo.jpg', 10),
(6, '../../Pictures/People/LeiaOrgana.jpg', 5),
(7, '../../Pictures/People/LukeSkywalker.jpg', 1),
(8, '../../Pictures/People/Obi-WanKenobi.jpg\n', 7),
(9, '../../Pictures/People/OwenLars.jpg', 6),
(10, '../../Pictures/People/R2-D2.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `climate`
--

CREATE TABLE `climate` (
  `id` int(3) UNSIGNED NOT NULL,
  `name` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `climate`
--

INSERT INTO `climate` (`id`, `name`) VALUES
(1, 'arid'),
(2, 'temperate'),
(3, 'tropical'),
(4, 'frozen'),
(5, 'murky'),
(6, 'windy'),
(7, 'hot'),
(8, 'humid');

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id` int(3) UNSIGNED NOT NULL,
  `title` varchar(50) NOT NULL,
  `release_date` date DEFAULT NULL,
  `episode` int(2) DEFAULT NULL,
  `opening` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `title`, `release_date`, `episode`, `opening`) VALUES
(1, 'A New Hope', '1977-05-25', 4, 'It is a period of civil war.\r\nRebel spaceships, striking\r\nfrom a hidden base, have won\r\ntheir first victory against\r\nthe evil Galactic Empire.\r\n\r\nDuring the battle, Rebel\r\nspies managed to steal secret\r\nplans to the Empire\'s\r\nultimate weapon, the DEATH\r\nSTAR, an armored space\r\nstation with enough power\r\nto destroy an entire planet.\r\n\r\nPursued by the Empire\'s\r\nsinister agents, Princess\r\nLeia races home aboard her\r\nstarship, custodian of the\r\nstolen plans that can save her\r\npeople and restore\r\nfreedom to the galaxy....'),
(2, 'The Empire Strikes Back', '1980-05-17', 5, 'It is a dark time for the\r\nRebellion. Although the Death\r\nStar has been destroyed,\r\nImperial troops have driven the\r\nRebel forces from their hidden\r\nbase and pursued them across\r\nthe galaxy.\r\n\r\nEvading the dreaded Imperial\r\nStarfleet, a group of freedom\r\nfighters led by Luke Skywalker\r\nhas established a new secret\r\nbase on the remote ice world\r\nof Hoth.\r\n\r\nThe evil lord Darth Vader,\r\nobsessed with finding young\r\nSkywalker, has dispatched\r\nthousands of remote probes into\r\nthe far reaches of space....'),
(3, 'Return of the Jedi', '1983-05-25', 6, 'Luke Skywalker has returned to\r\nhis home planet of Tatooine in\r\nan attempt to rescue his\r\nfriend Han Solo from the\r\nclutches of the vile gangster\r\nJabba the Hutt.\r\n\r\nLittle does Luke know that the\r\nGALACTIC EMPIRE has secretly\r\nbegun construction on a new\r\narmored space station even\r\nmore powerful than the first\r\ndreaded Death Star.\r\n\r\nWhen completed, this ultimate\r\nweapon will spell certain doom\r\nfor the small band of rebels\r\nstruggling to restore freedom\r\nto the galaxy...'),
(4, 'The Phantom Menace', '1999-05-19', 1, 'Turmoil has engulfed the\r\nGalactic Republic. The taxation\r\nof trade routes to outlying star\r\nsystems is in dispute.\r\n\r\nHoping to resolve the matter\r\nwith a blockade of deadly\r\nbattleships, the greedy Trade\r\nFederation has stopped all\r\nshipping to the small planet\r\nof Naboo.\r\n\r\nWhile the Congress of the\r\nRepublic endlessly debates\r\nthis alarming chain of events,\r\nthe Supreme Chancellor has\r\nsecretly dispatched two Jedi\r\nKnights, the guardians of\r\npeace and justice in the\r\ngalaxy, to settle the conflict....'),
(5, 'Attack of the Clones', '2002-05-16', 2, 'There is unrest in the Galactic\r\nSenate. Several thousand solar\r\nsystems have declared their\r\nintentions to leave the Republic.\r\n\r\nThis separatist movement,\r\nunder the leadership of the\r\nmysterious Count Dooku, has\r\nmade it difficult for the limited\r\nnumber of Jedi Knights to maintain \r\npeace and order in the galaxy.\r\n\r\nSenator Amidala, the former\r\nQueen of Naboo, is returning\r\nto the Galactic Senate to vote\r\non the critical issue of creating\r\nan ARMY OF THE REPUBLIC\r\nto assist the overwhelmed\r\nJedi....'),
(6, 'Revenge of the Sith', '2005-05-19', 3, 'War! The Republic is crumbling\r\nunder attacks by the ruthless\r\nSith Lord, Count Dooku.\r\nThere are heroes on both sides.\r\nEvil is everywhere.\r\n\r\nIn a stunning move, the\r\nfiendish droid leader, General\r\nGrievous, has swept into the\r\nRepublic capital and kidnapped\r\nChancellor Palpatine, leader of\r\nthe Galactic Senate.\r\n\r\nAs the Separatist Droid Army\r\nattempts to flee the besieged\r\ncapital with their valuable\r\nhostage, two Jedi Knights lead a\r\ndesperate mission to rescue the\r\ncaptive Chancellor....');

-- --------------------------------------------------------

--
-- Table structure for table `filmplanets`
--

CREATE TABLE `filmplanets` (
  `id_film` int(3) UNSIGNED NOT NULL,
  `id_planet` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `filmplanets`
--

INSERT INTO `filmplanets` (`id_film`, `id_planet`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 1),
(3, 5),
(3, 7),
(3, 8),
(3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `hasclimate`
--

CREATE TABLE `hasclimate` (
  `id_planet` int(3) UNSIGNED NOT NULL,
  `id_climate` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hasclimate`
--

INSERT INTO `hasclimate` (`id_planet`, `id_climate`) VALUES
(1, 1),
(2, 2),
(3, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(12, 6),
(13, 7),
(14, 3),
(15, 7),
(15, 8),
(16, 2);

-- --------------------------------------------------------

--
-- Table structure for table `News_Pictures`
--

CREATE TABLE `News_Pictures` (
  `id` int(20) NOT NULL,
  `Lien` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `News_Pictures`
--

INSERT INTO `News_Pictures` (`id`, `Lien`) VALUES
(1, '../../Pictures/News/Alexandre.gif'),
(2, '../../Pictures/News/Sam.jpg\r\n'),
(3, '../../Pictures/News/Deux.jpg\r\n'),
(4, '../../Pictures/News/tropmims.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` char(1) DEFAULT NULL,
  `height` int(3) DEFAULT NULL,
  `mass` int(4) DEFAULT NULL,
  `homeworld` int(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `name`, `gender`, `height`, `mass`, `homeworld`) VALUES
(1, 'Luke Skywalker', 'M', 172, 77, 1),
(2, 'C-3PO', '-', 167, 75, 1),
(3, 'R2-D2', '-', 96, 32, 8),
(4, 'Darth Vader', 'M', 202, 136, 1),
(5, 'Leia Organa', 'F', 150, 49, 2),
(6, 'Owen Lars', 'M', 178, 120, 1),
(7, 'Obi-Wan Kenobi', 'M', 182, 77, 16),
(8, 'Anakin Skywalker', 'M', 188, 84, 1),
(9, 'Chewbacca', 'M', 228, 112, 14),
(10, 'Han Solo', 'M', 180, 80, 17);

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `id_picture` int(6) NOT NULL,
  `id` int(6) UNSIGNED NOT NULL,
  `path` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`id_picture`, `id`, `path`) VALUES
(1, 4, '../../Pictures/Films/Film1_poster.jpg'),
(2, 5, '../../Pictures/Films/Film2_poster.jpg'),
(3, 6, '../../Pictures/Films/Film3_poster.jpg'),
(5, 2, '../../Pictures/Films/Film5_poster.jpg'),
(6, 3, '../../Pictures/Films/Film6_poster.jpg'),
(10, 1, '../../Pictures/Films/Film4_poster.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `planet`
--

CREATE TABLE `planet` (
  `id` int(3) UNSIGNED NOT NULL,
  `name` varchar(35) NOT NULL,
  `diameter` int(7) DEFAULT NULL,
  `population` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `planet`
--

INSERT INTO `planet` (`id`, `name`, `diameter`, `population`) VALUES
(1, 'Tatooine', 10465, '200000'),
(2, 'Alderaan', 12500, '2000000000'),
(3, 'Yavin IV', 10200, '1000'),
(4, 'Hoth', 7200, 'unknown'),
(5, 'Dagobah', 8900, 'unknown'),
(6, 'Bespin', 118000, '6000000'),
(7, 'Endor', 4900, '30000000'),
(8, 'Naboo', 12120, '4500000000'),
(9, 'Coruscant', 12240, '1000000000000'),
(10, 'Kamino', 19720, '1000000000'),
(11, 'Geonosis', 11370, '100000000000'),
(12, 'Utapau', 12900, '95000000'),
(13, 'Mustafar', 4200, '20000'),
(14, 'Kashyyyk', 12765, '45000000'),
(15, 'Felucia', 9100, '8500000'),
(16, 'Stewjon', 0, 'unknown'),
(17, 'Corellia', 11000, '3000000000');

-- --------------------------------------------------------

--
-- Table structure for table `playsin`
--

CREATE TABLE `playsin` (
  `id_people` int(6) UNSIGNED NOT NULL,
  `id_film` int(4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `playsin`
--

INSERT INTO `playsin` (`id_people`, `id_film`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 6),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(7, 1),
(8, 4),
(8, 5),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 2),
(10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id_user` int(6) NOT NULL,
  `id_film` int(3) UNSIGNED NOT NULL,
  `review` varchar(50) NOT NULL,
  `comment` varchar(1000) DEFAULT NULL,
  `date` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id_user`, `id_film`, `review`, `comment`, `date`) VALUES
(2, 4, 'Excellent', '', '15/11/2020 02:42'),
(5, 6, 'Very Bad', 'Je ne suis pas d\'accord avec toi, le meilleur GUERRE DES ETOILES c\'est le GUERRE DES ETOILES 9 avec RAY DU CUL', '20/11/2020 11:55'),
(10, 5, 'Perfect', 'Excellent film ! et site vraiment incroyable !', '25/11/2020 02:00');

-- --------------------------------------------------------

--
-- Table structure for table `species`
--

CREATE TABLE `species` (
  `id` int(3) UNSIGNED NOT NULL,
  `classification` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `species`
--

INSERT INTO `species` (`id`, `classification`, `name`) VALUES
(1, 'mammal', 'Human'),
(2, 'artificial', 'Droid'),
(3, 'mammal', 'Wookie'),
(4, 'reptilian', 'Rodian'),
(5, 'gastropod', 'Hutt'),
(6, 'mammal', 'Yoda\'s species'),
(7, 'reptile', 'Trandoshan'),
(8, 'amphibian', 'Mon Calamari'),
(9, 'mammal', 'Ewok'),
(10, 'mammal', 'Sullustan'),
(11, 'unknown', 'Neimodian'),
(12, 'amphibian', 'Gungan'),
(13, 'mammal', 'Toydarian'),
(14, 'mammal', 'Dug'),
(15, 'mammal', 'Twi\'lek'),
(16, 'reptile', 'Aleena'),
(17, 'unknown', 'Vulptereen');

-- --------------------------------------------------------

--
-- Table structure for table `starship`
--

CREATE TABLE `starship` (
  `id` int(3) UNSIGNED NOT NULL,
  `class` varchar(40) NOT NULL,
  `mglt` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `starship`
--

INSERT INTO `starship` (`id`, `class`, `mglt`) VALUES
(1, 'corvette', 60),
(2, 'Star Destroyer', 60),
(3, 'landing craft', 70),
(4, 'Deep Space Mobile Battlestation', 10),
(5, 'Light freighter', 75),
(6, 'assault starfighter', 80),
(7, 'Starfighter', 100),
(8, 'Armed government transport', 50);

-- --------------------------------------------------------

--
-- Table structure for table `starshipsinfilms`
--

CREATE TABLE `starshipsinfilms` (
  `id_starship` int(3) UNSIGNED NOT NULL,
  `id_film` int(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(6) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `birthday` date NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(120) NOT NULL,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `first_name`, `last_name`, `birthday`, `username`, `email`, `password`, `role`) VALUES
(1, 'admin', 'admin', '2020-11-05', 'admin', 'admin@esigelec.fr', '$2y$10$aZ7.tQFfY81U4.vgS3SIju3WapP0xPg1WpGceA35NExhwRZ1kUZjO', 3),
(2, 'Mourad', 'Latoundji', '2020-11-10', 'Myster', 'mouradlatoundji@gmail.com', '$2y$10$v94ACaOWTaXlQUjaV/90pOwmfJAYimQ.jvi2d.dMqNFeS5hFkv4Hy', 1),
(3, 'Austin', 'Morel', '2020-11-15', 'Austino', 'austin@hotmail.fr', '$2y$10$sQZWdWIWjiKrIj0q2ynihOCNX.kDMu/0JQ2YmcsN2pGRjC5pNIvjW', 1),
(4, 'a', 'a', '1111-11-11', 'a', 'a@a.a', '$2y$10$9B7YELuCwNBnq4yPWGPDQOe2fYfPDd6gbpJ7p4UbgwSdhk3aTitBW', 1),
(5, 'JOT', 'WW', '2000-01-01', 'JOT', 'JOT@gmail.com', '$2y$10$llet5QdFx7JZa1RJKnz6GeCBhL5aTAYTPSVFCdJrOuJvQanfnHYie', 1),
(7, 'Momo', 'Momo', '2020-11-08', 'Momo', 'momo@gmail.com', '$2y$10$TKwpH5Bi5dDF6awg3./1ROZeEB.hVPaZoNNA27kL8/mCGWUhA2Z8K', 1),
(8, 'Claire', 'Leguillette', '1998-03-30', 'Claire', 'claire.leguillette@gbcvbc', '$2y$10$2lhfSn4qLEndlzeNf9o3KubFiRlwfsmK1lB3NA6zxZDk.QbgwlUZC', 1),
(9, 'gfh', 'dfg', '1111-11-11', 'test', 'leguillette.claire@gmail.com', '$2y$10$LIxHGjKbmb7mmMUFSSkTgO0ixL9B3tRDpwcLm/VVtckKQfhQ.RvNO', 1),
(10, 'benoit', 'simon', '0001-01-01', 'ben', 'b@b.b', '$2y$10$lZLrIN08h6Y090/JqnVdcuemAZy5FbA2WUiSVI/Vqv6tslizt496u', 1),
(11, ';DROP TABLE film --', 'asas', '0013-02-21', 'aa6546464', 'zdjnazod@zapjdp.Zdadzad', '$2y$10$mOkY.1PYyPIBXAyTkeTSxuXjEup6OOngDa3yLUjFAoxs7SBhgoZM.', 1),
(12, 'zhang', 'xuxu', '2033-11-25', 'woniubi', 'nishabi@eee.com', '$2y$10$0/6eF2ustRDwNYhZ2wX2leaOk1guGys.z2f7S530oEDp3CLTAM/6u', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Charater_Pictures`
--
ALTER TABLE `Charater_Pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `climate`
--
ALTER TABLE `climate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filmplanets`
--
ALTER TABLE `filmplanets`
  ADD PRIMARY KEY (`id_film`,`id_planet`),
  ADD KEY `fk_filmplanets_planet` (`id_planet`);

--
-- Indexes for table `hasclimate`
--
ALTER TABLE `hasclimate`
  ADD PRIMARY KEY (`id_planet`,`id_climate`),
  ADD KEY `fk_hasclimate_climate` (`id_climate`);

--
-- Indexes for table `News_Pictures`
--
ALTER TABLE `News_Pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_people_homeworld` (`homeworld`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id_picture`,`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `planet`
--
ALTER TABLE `planet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `playsin`
--
ALTER TABLE `playsin`
  ADD PRIMARY KEY (`id_people`,`id_film`),
  ADD KEY `fk_playsin_film` (`id_film`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id_user`,`id_film`),
  ADD KEY `id_film` (`id_film`);

--
-- Indexes for table `species`
--
ALTER TABLE `species`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `starship`
--
ALTER TABLE `starship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `starshipsinfilms`
--
ALTER TABLE `starshipsinfilms`
  ADD PRIMARY KEY (`id_starship`,`id_film`),
  ADD KEY `fk_starshipinfilms_film` (`id_film`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `Id_user` (`id_user`),
  ADD UNIQUE KEY `Id_user_2` (`id_user`),
  ADD UNIQUE KEY `pseudo` (`username`,`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `climate`
--
ALTER TABLE `climate`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id_picture` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `planet`
--
ALTER TABLE `planet`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `species`
--
ALTER TABLE `species`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `starship`
--
ALTER TABLE `starship`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `filmplanets`
--
ALTER TABLE `filmplanets`
  ADD CONSTRAINT `fk_filmplanets_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `fk_filmplanets_planet` FOREIGN KEY (`id_planet`) REFERENCES `planet` (`id`);

--
-- Constraints for table `hasclimate`
--
ALTER TABLE `hasclimate`
  ADD CONSTRAINT `fk_hasclimate_climate` FOREIGN KEY (`id_climate`) REFERENCES `climate` (`id`),
  ADD CONSTRAINT `fk_hasclimate_planet` FOREIGN KEY (`id_planet`) REFERENCES `planet` (`id`);

--
-- Constraints for table `people`
--
ALTER TABLE `people`
  ADD CONSTRAINT `fk_people_homeworld` FOREIGN KEY (`homeworld`) REFERENCES `planet` (`id`);

--
-- Constraints for table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`id`) REFERENCES `film` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `playsin`
--
ALTER TABLE `playsin`
  ADD CONSTRAINT `fk_playsin_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `fk_playsin_people` FOREIGN KEY (`id_people`) REFERENCES `people` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `starshipsinfilms`
--
ALTER TABLE `starshipsinfilms`
  ADD CONSTRAINT `fk_starshipinfilms_film` FOREIGN KEY (`id_film`) REFERENCES `film` (`id`),
  ADD CONSTRAINT `fk_starshipinfilms_starship` FOREIGN KEY (`id_starship`) REFERENCES `starship` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
